" Make vim portable.
if ($OS == 'Windows_NT')
    " Windows specific settings
    let $HOME=$VIM
endif

set nocompatible
" do not load mswin, since it changes Ctrl+F to find files instead of PageDown
"source $VIMRUNTIME/mswin.vim
"behave mswin

set nobackup
set noundofile
set nowritebackup

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      if empty(&shellxquote)
        let l:shxq_sav = ''
        set shellxquote&
      endif
      let cmd = '"' . $VIMRUNTIME . '\diff"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3
  if exists('l:shxq_sav')
    let &shellxquote=l:shxq_sav
  endif
endfunction


set relativenumber
function! NumberToggle()
  if(&relativenumber == 1)
    set number
    set norelativenumber
  else
    set nonumber
    set relativenumber
  endif
endfunc

nnoremap <F7> :call NumberToggle()<cr>

call plug#begin('~/vimfiles/bundles')
Plug 'joshdick/onedark.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mhinz/vim-signify'
"Plug 'tpope/vim-fugitive'
Plug 'https://github.com/enterit/vim-fugitive.git'
Plug 'junegunn/gv.vim'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'
Plug 'jiangmiao/auto-pairs'
Plug 'maxbrunsfeld/vim-yankstack'
Plug 'tpope/vim-surround'
Plug 'terryma/vim-expand-region'
Plug 'lyokha/vim-xkbswitch'
Plug 'mileszs/ack.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'drmikehenry/vim-fontsize'
Plug 'ivalkeen/nerdtree-execute'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
call plug#end()

if !has("gui_running")
    set term=xterm
    let &t_AB="\e[48;5;%dm"
    let &t_AF="\e[38;5;%dm"
endif

if ($TERM == 'xterm-256color')
    set t_Co=256
    let g:onedark_termcolors=256
else
    set t_Co=16
    let g:onedark_termcolors=16
endif

colorscheme onedark
let g:airline_theme='onedark'
" replace default light-blue backround of the selected line in the quickfix pane with a dark color
" other good colors to try: #181A1F, #2C323C, #3E4452
:hi QuickFixLine guibg=#181A1F


" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
autocmd Filetype javascript     setlocal ts=2 sts=2 sw=2
autocmd Filetype css            setlocal ts=2 sts=2 sw=2
autocmd Filetype json           setlocal ts=2 sts=2 sw=2
autocmd Filetype yaml           setlocal ts=2 sts=2 sw=2
autocmd Filetype jenkinsfile    setlocal ts=2 sts=2 sw=2


" set default font
if ($OS == 'Windows_NT')
    " Windows specific settings
    set guifont=DejaVu\ Sans\ Mono\ for\ Powerline:h10
else
    " Unix specific settings
    set guifont=Roboto\ Mono\ for\ Powerline\ 10
endif


" setting laststatus forces bottom status line to be shown
set laststatus=2
" utf-8 is required for special airline characters
set encoding=utf8

" air-line
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

let g:airline_symbols = {}
" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.maxlinenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline#extensions#tabline#enabled = 1

" set fold level to high number, so that folds would not be collapsed by defaults
set foldlevelstart=99


" update gutter (line status) if file was modified
" this is a workaround for signify not refresing
au BufLeave * if &mod | SignifyRefresh | endif
au FocusLost * if &mod | SignifyRefresh | endif


" autosave on buffer switch and focus lost
au BufLeave * silent! upd
au FocusLost * silent! wall


nnoremap <Tab> <c-w>w
nnoremap <bs> <c-w>W


" Ctrl+S to save file in normal, visual and insert modes
noremap <silent> <C-S>          :update<CR>
vnoremap <silent> <C-S>         <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>


set ignorecase
set smartcase


" set default text width to 120 characters, 80 is too small for wide screens
set textwidth=120


" command to review all stashed changes
command Greview :Git! diff --cached


" open vim in full-screen mode
au GUIEnter * simalt ~x
" setting colums to high value alows to avoid white scroll bar shown briefly in the middle of the screen on gvim open
set columns=200


" signify plugin settings
let g:signify_sign_show_count = 0
let g:signify_sign_change = '~'
let g:signify_vcs_list = [ 'git' ]


" Use space bar as a leader key
let mapleader = "\<Space>"
nnoremap <Space> <Nop>
map <Del> <Space>
map <Leader>c<Del> <Plug>NERDCommenterInvert


" Do not move cursor when switching tabs
set nostartofline


" ack plugin settings for search using ag

"let g:ackprg = 'ag --nogroup --nocolor --column'
let g:ackprg = 'rg --vimgrep --no-heading'
let g:ackhighlight = 1

" key bindings for ack search
nnoremap <leader>a :Ack
vnoremap <Leader>a y:Ack <C-r>=shellescape(@")<CR><CR>


" CtrlP plugin settings

" Setup some default ignores
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn)|\_site)$',
  \ 'file': '\v\.(exe|so|dll|class|png|jpg|jpeg)$',
\}

" Use the nearest .git directory as the cwd
" This makes a lot of sense if you are working on a project that is in version
" control. It also supports works with .svn, .hg, .bzr.
let g:ctrlp_working_path_mode = 'r'

" Use a leader instead of the actual named binding
nmap <leader>o :CtrlP<cr>

" Easy bindings for its various modes
nmap <leader>bb :CtrlPBuffer<cr>
nmap <leader>bm :CtrlPMixed<cr>
nmap <leader>bs :CtrlPMRU<cr>


" Buffer key bindings

" This allows buffers to be hidden if you've modified a buffer.
" This is almost a must if you wish to use buffers in this way.
set hidden

" To open a new empty buffer
" This replaces :tabnew which I used to bind to this mapping
nmap <leader>T :enew<cr>

" Move to the next buffer
nmap <leader>l :bnext<CR>

" Move to the previous buffer
nmap <leader>h :bprevious<CR>

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd #<CR>

" Show all open buffers and their status
nmap <leader>bl :ls<CR>


" show popup window on the bottom, otherwise it shows up at the top and shifts current buffer down
set splitbelow


" key bindings for vim-expand-region
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)


" Fugitive settings

" Use gvim for fugitive when it is called from gvim.
" Otherwise vim is called from Git MSYS console, which breaks xterm colors.
"if has("gui_running")
    "let g:fugitive_git_executable = 'set GIT_EDITOR=gvim && git'
"endif

" key bindings for fugitive
map <F3> :Gst<CR>


" key bindings for gv
map <F2> :GV --all<CR>


" NERDTree settings

" key bindings for NERDTree
nmap <silent> <Leader>p :NERDTreeToggle<CR>
" disable left-hand scroll in gui vim, since it causes gvim window to jump and resize
" when NERDTree shows up
:set guioptions-=L

" collapse/expand characters
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'


" Map the Y key to y$, so that it behaves the same as D and C.
" This should happen after yankstack plugin is initialized.
call yankstack#setup()
nmap Y y$
" Make it possible to use Alt-.. combinations in gvim. By default they are bound to menus.
set winaltkeys=no
" Disable autopairs Alt-P mapping, so it could be used by yankstack plugin.
let g:AutoPairsShortcutToggle = ''


" Highlight customizations

" higlight all matches
:set hlsearch

" key bindings for search highlight removing
map <silent> <leader><cr> :noh<cr>


function! VisualSelection(direction, extra_filter) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'gv'
        call CmdLine("Ag \"" . l:pattern . "\" " )
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

""""""""""""""""""""""""""""""
" => Visual mode related
""""""""""""""""""""""""""""""
" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>


" select pasted text
nnoremap gp `[v`]


" https://github.com/mxw/vim-jsx
let g:jsx_ext_required = 0


" better support of languages:
" https://github.com/lyokha/vim-xkbswitch
" https://github.com/DeXP/xkb-switch-win
let g:XkbSwitchEnabled = 1
let g:XkbSwitchIMappings = ['ru']
let g:XkbSwitchDynamicKeymap = 1
let g:XkbSwitchKeymapNames = {'ru' : 'russian-jcukenwin'}
let g:XkbSwitchAssistNKeymap = 1    " for commands r and f
let g:XkbSwitchAssistSKeymap = 1    " for search lines


" allow the . to execute once for each line of a visual selection
vnoremap . :normal .<CR>


" prevent vim from being slow on Ubuntu 14.04 (on Windows 10)
" https://stackoverflow.com/questions/9341768/vim-response-quite-slow
set regexpengine=1


" copy full git revision from GV tree or Fugitive buffer
function Rev()
  if gv#sha() != ''
    return fugitive#buffer().repo().rev_parse(gv#sha())
  else
    return fugitive#buffer().sha1()
  endif
endfunction

nnoremap <silent> y<C-R> :call setreg(v:register, Rev())<CR>


" Yank to clipboard by default.
" This has to be done after all the buffer-related key bindings are set up, otherwise they seem to use a different
" default buffer.
set clipboard=unnamed
